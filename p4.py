import csv
import matplotlib.pyplot as plt
from collections import OrderedDict


top_economical_bowler_in_2015 = {}
id_2015 = []
with open('matches.csv') as csv_file:
	matches = csv.DictReader(csv_file)
	for match in matches:
		if match['season'] == '2015':
			id_2015.append(match['id'])


with open('deliveries.csv') as csv_file:
	deliveries = csv.DictReader(csv_file)
	for delivery in deliveries:
		if delivery['match_id'] in id_2015:
			bowler = delivery.get("bowler")
			total_runs = int(delivery.get('total_runs'))
			total_balls = int(delivery.get('ball')) / 6
			avg =  total_runs / total_balls
			top_economical_bowler_in_2015[bowler] = top_economical_bowler_in_2015.get(bowler, 0) + avg

top_economical_bowler_in_2015_sorted = OrderedDict(sorted(top_economical_bowler_in_2015.items(), key=lambda t: t[1]))
top_ten = {k: top_economical_bowler_in_2015_sorted[k] for k in list(top_economical_bowler_in_2015_sorted)[:10]}

Name = list(top_ten.keys())
Econ = list(top_ten.values())
plt.bar(range(len(top_ten)), Econ, tick_label=Name)
plt.xticks(rotation="80")
plt.title("Top 10 economical bowler in 2015 ")
plt.subplots_adjust(bottom=0.3)
plt.xlabel('Bowler Name')
plt.ylabel('Economy')
plt.show()