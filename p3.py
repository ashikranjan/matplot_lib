import csv
import matplotlib.pyplot as plt

per_team_extra_in_2016 = {}
id_2016 = []

with open('matches.csv') as csv_file:
    matches = csv.DictReader(csv_file)
    for match in matches:
        if match['season'] == '2016':
            id_2016.append(match['id'])


with open('deliveries.csv') as csv_file:
    deliveries = csv.DictReader(csv_file)
    for delivery in deliveries:
        match_id = delivery.get('match_id')
        if match_id in id_2016:
            extra_runs = int(delivery.get('extra_runs'))
            bowling_team = delivery.get("bowling_team")
            per_team_extra_in_2016[bowling_team] = per_team_extra_in_2016.get(bowling_team, 0) + extra_runs

t_name = list(per_team_extra_in_2016.keys())
N_times = list(per_team_extra_in_2016.values())
plt.bar(range(len(per_team_extra_in_2016)), N_times, tick_label=t_name, color='#f12765')

plt.xlabel('Team Name')
plt.xticks(rotation="75")
plt.subplots_adjust(bottom=0.3)
plt.ylabel('Extra Runs')
plt.title("Extra runs conceded per Team in year 2016")
plt.show()
