import csv
import matplotlib.pyplot as plt


matche_won_per_year_per_team = {}
with open('matches.csv') as csv_file:
    matches = csv.DictReader(csv_file)
    for match in matches:
        season = match.get('season')
        winner = match.get('winner')
        old_season_result = matche_won_per_year_per_team.get(season, dict())
        matche_won_per_year_per_team[season] = old_season_result
        if winner != '':
            matche_won_per_year_per_team[season][winner] = matche_won_per_year_per_team[season].get(winner, 1) + 1


colors = ['#4E3530', '#30434E', '#28A1E7', '#EE6A19', '#EED119',
          '#6DEE19', '#665C66', '#ED1C45', '#ED1C1F', '#9879A6', '#b455d3']
plot = []

for year, winner in sorted(matche_won_per_year_per_team.items()):
    offset_value = 0
    color_iterator = 0
    for team in sorted(winner):
        p = plt.bar(int(year), winner.get(team), width=0.4,
                    bottom=offset_value, color=colors[color_iterator])
        plot.append(p[0])
        offset_value += winner.get(team)
        color_iterator += 1

plt.title('Number of match won by all the team over all the year')
plt.ylabel('Number of match')
plt.xlabel('Year')
plt.legend(plot, sorted(winner))
plt.show()
