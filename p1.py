import csv
from matplotlib import pyplot as plt
from collections import OrderedDict

match_per_season = {}

with open('matches.csv') as csv_file:
    csv_file = csv.DictReader(csv_file)
    for matches in csv_file:
    	season = matches.get('season')
    	match_per_season[season] = match_per_season.get(season, 0) + 1

match_per_season_sorted = OrderedDict(sorted(match_per_season.items()))
year = list(match_per_season_sorted.keys())
matches = list(match_per_season_sorted.values())
plt.bar(range(len(match_per_season_sorted)), matches, tick_label=year, width=0.5, color='#a23454')
plt.xlabel('Year')
plt.ylabel('Match')
plt.title("Match per Year")
plt.show()
